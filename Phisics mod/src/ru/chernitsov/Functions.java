package ru.chernitsov;

public class Functions {


    private double U1;
    private double U2;
    private double U3;
    private double U4;
    private Quaternion g = new Quaternion(0, 0, 0, -9.8);
    public Functions(double f1, double f2, double f3, double f4, Copter c) {
        calcU1(f1, f2, f3, f4);
        calcU2_4(f1, f2, f3, f4, c);
    }

    public void calcU1(double f1, double f2, double f3, double f4){
        this.U1 = f1 + f2 + f3 + f4;
    }
    public void calcU2_4(double f1, double f2, double f3, double f4, Copter c){
        this.U2 = Math.sqrt(2)/2 * c.l * (f3 - f2 - f1 + f4);
        this.U3 = Math.sqrt(2)/2 * c.l * (f1 - f2 - f3 + f4);
        this.U4 = f2 - f1 - f3 + f4;
    }
    public double[] linearVel(Copter c) {
        double[] vel = new double[3];
        Quaternion q = Quaternion.Vec4d2Qternion(Matrix.cutOutOf(9, 4, c.state));
        vel = Quaternion.quat2vec(Quaternion.multiplication(Quaternion.multiplication(q, g), Quaternion.inverse(q)));
        vel[2] += 1/c.m * U1;
        return vel;
    }
    public double[] angularVel(double[] ang, Copter c){
        ang[0] = U2 / c.tenzorOfInertion[0];
        ang[1] = U3 / c.tenzorOfInertion[1];
        ang[2] = U4 / c.tenzorOfInertion[2];
        return ang;
    }
    public double[] position(Copter c, double[] vel) {
        Quaternion q = Quaternion.Vec4d2Qternion(Matrix.cutOutOf(9, 4, c.state));
        return Quaternion.quat2vec(Quaternion.multiplication(Quaternion.multiplication(q, Quaternion.vec2quat(vel)), Quaternion.inverse(q)));
    }
    public double[] quaternion(double[] angulsrVel, Copter c){
        Quaternion q = Quaternion.Vec4d2Qternion(Matrix.cutOutOf(9, 4, c.state));
        return Quaternion.Qternion2Vek4(Quaternion.localDerivation(angulsrVel, q));
    }
}
