package ru.chernitsov;

import static java.lang.Math.sqrt;

public class Quaternion extends Matrix{


    public Quaternion (double r, double i, double j, double k){
        super(4,1);
        this.setElem(0,0, r);
        this.setElem(1,0, i);
        this.setElem(2,0, j);
        this.setElem(3,0, k);
    }

    public double getR() {
        return getElem(0,0);
    }
    public void setR(double r) {
        this.setElem(0,0, r);
    }

    public double getI() {
        return getElem(1,0);
    }

    public void setI(double i) {
        this.setElem(1,0, i);
    }

    public double getJ() {
        return getElem(2,0);
    }

    public void setJ(double j) {
        this.setElem(2,0, j);
    }

    public double getK() {
        return getElem(3,0);
    }

    public void setK(double k) {
        this.setElem(3,0, k);
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    public static Quaternion multOnSk(Quaternion one, double sk) {
        return new Quaternion(one.getR() * sk,one.getI() * sk,one.getJ() * sk,one.getK() * sk);
    }

    // умножение кватов
    public static Quaternion multiplication(Quaternion first, Quaternion second) { // first * second
        Quaternion answ = new Quaternion(0,0,0,0);
        double box = 0.0;
        double fr = first.getR();
        double fi = first.getI();
        double fj = first.getJ();
        double fk = first.getK();
        double sr = second.getR();
        double si = second.getI();
        double sj = second.getJ();
        double sk = second.getK();
        box = fr * sr - fi * si - fj * sj - fk * sk;
        answ.setR(box);
        box = fr * si + fi * sr + fj * sk - fk * sj ;
        answ.setI(box);
        box = fr * sj + fj * sr + fk * si - fi * sk ;
        answ.setJ(box);
        box = fr * sk + fk * sr + fi * sj - fj * si;
        answ.setK(box);
        return answ;
    }
    public static Quaternion vec2quat(double[] vec){
        return new Quaternion(0,vec[0],vec[1],vec[2]);
    }
    public static double[] quat2vec(Quaternion q){
        double[] answ = new double[3];
        answ[0] = q.getI();
        answ[1] = q.getJ();
        answ[2] = q.getK();
        return answ;
    }

    //обратный
    public static Quaternion inverse(Quaternion quaternion){
        double len = norm(quaternion);
        Quaternion it = conjugate(quaternion);
        return new Quaternion(it.getR()/len,it.getI()/len,it.getJ()/len,it.getK()/len);
    }
    public static double norm (Quaternion it){
        return sqr(it.getR()) + sqr(it.getI()) + sqr(it.getJ()) + sqr(it.getK());
    }

    private static double sqr(double r) {
        return r * r;
    }
    public static Quaternion conjugate(Quaternion quat){
        return  new Quaternion(quat.getR(),-1 * quat.getI(),-1 * quat.getJ(),-1 * quat.getK());
    }


    // поворот вектора
    public static Quaternion vecTurn(double[] vec, Quaternion quat) {
        return multiplication(multiplication(quat, vec2quat(vec)), inverse(quat));
    }
    public static Quaternion backVecTurn(double[] vec, Quaternion quat) {
        return  multiplication(multiplication(inverse(quat), vec2quat(vec)), quat);
    }
    // дифиринцирование
    public static Quaternion globalDerivation (double[] angVel, Quaternion one) {
        Quaternion Vel = vec2quat(angVel);
        return multiplication(multOnSk(Vel, 0.5), one);
    }

    public static Quaternion localDerivation (double[] angVel, Quaternion one) {
        Quaternion Vel = vec2quat(angVel);
        return multiplication(multOnSk(one, 0.5), Vel);
    }
    public static Quaternion Vec4d2Qternion(double[] vec) {
        return new Quaternion(vec[0], vec[1], vec[2], vec[3]);
    }
    public static double[] Qternion2Vek4(Quaternion q) {
        double[] t = {q.getR(), q.getI(), q.getJ(), q.getK()};
        return t;
    }
}
