package ru.chernitsov;

import java.util.Scanner;

import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.sin;

public class Main {

    long startTime=System.currentTimeMillis();

    public double getTime() {
        return (System.currentTimeMillis()-startTime) / 1000.f;
    }

    public Main() throws InterruptedException {
        IntegratorRK integratorRK = new IntegratorRK(0);
        //Integrator integrator = new Integrator(0);
        double lastT = getTime();
        while (getTime() < 3) {
            System.out.print(getTime());
            System.out.print(" : " + integratorRK.step(getTime() - lastT, getTime()));
            //System.out.println(" : " + integrator.step(getTime() - lastT, getTime()));
            lastT = getTime();
            Thread.sleep(10);
        }


    }

    public static void main(String[] args) throws InterruptedException {
        new Main();
    }
}