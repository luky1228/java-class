package ru.chernitsov;

public class IntegratorRK {
    double alpha = 1;
    double betta = 1;
    double a = 0.5;
    double b = 0.5;
    double state;
    public IntegratorRK(double init) {
        state = init;
    }
    public double step(double h, double t){
        double k1 = h * func(state, t);
        double k2 = h * func(state + k1,t + h);
        state +=  (k1 + k2) / 2;
        return state;
    }

    public double func(double y, double t){
        return Math.sin(t);
    }
}
