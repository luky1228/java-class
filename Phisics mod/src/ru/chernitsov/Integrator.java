package ru.chernitsov;




public class Integrator {
    Copter state;
    public Integrator (Copter initial){
        state = initial;
    }
    public Copter step(double h, double t) {
        Functions f = new Functions(0, 0, 0, 0, state);
        state.state.addElems1D(6, f.angularVel(Matrix.cutOutOf(6, 3, state.state), state), h);
        state.state.addElems1D(9, f.quaternion(Matrix.cutOutOf(9, 4, state.state), state), h);
        state.state.addElems1D(3, f.linearVel(state), h);
        state.state.addElems1D(0, f.position(state, Matrix.cutOutOf(6, 3, state.state)), h);
        return state;
    }

}
