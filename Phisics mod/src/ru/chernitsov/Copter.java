package ru.chernitsov;

public class Copter {
    double m;
    double l;
    Matrix state = new Matrix(13, 1);
    double[] tenzorOfInertion = new double[3];
    public Copter(double m, double l, double[] eta, double[] v, double[] omega, double[] I, double[] lambda){
        this.m = m;

        this.l = l;
        for (int i = 0; i < 3; ++i) {
            this.state.addElems1D(0, eta);
            this.state.addElems1D(3, v);
            this.state.addElems1D(6,omega);
            this.state.addElems1D(9, lambda);
            tenzorOfInertion[i] = m;
        }
        this.state.setElem(12, 1, lambda[3]);
        this.tenzorOfInertion = I;
    }

}
