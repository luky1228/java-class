package transport;

public class Boat extends Water_transport implements turn_over {
    public boolean is_compact;
    public double speed;
    public boolean will_turn_over() {
        if (speed > 200) {
            return true;
        }
        return false;
    }
}
